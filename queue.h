#ifndef QUEUE_H
#define QUEUE_H

/*
 * This is simple implementation of a circular buffer.
 * It does not contain Dequeue() routine as it was not
 * required for complition of this task.
 */

struct Queue
{
   int maxSize; // Total size of the buffer
   int size;    // Number of elements in the buffer
   int head;
   int tail;
   float * samples;
};

struct Queue * createQueue(int maxSize);

void destroyQueue(struct Queue * queue);

int Enqueue(struct Queue * queue, float sample);

int showQueue(struct Queue * queue);
#endif /* QUEUE_H */
