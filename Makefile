CC = gcc
CCFLAGS = -Wall
LIBS = -lpthread -lm

main: 
	$(CC) $(CCFLAGS) main.c queue.c $(LIBS)
