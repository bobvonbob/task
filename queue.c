#include <stdio.h>
#include <stdlib.h>
#include "queue.h"

/*
 ********************************************************
 * Function: createQueue
 *
 * params:
 *    maxSize - how many samples queue can contain
 *
 * returns:
 *    NULL if fail, pointer to queue on success
 *
 ********************************************************
 */
struct Queue * createQueue(int maxSize)
{
   if( maxSize < 1) {
      fprintf(stderr, "Queue size cannot be < 1\n");
      return NULL;
   }

   struct Queue * queue;

   /*
    * C standard does not require explicit cast of
    * malloc() result. It is required by C++. Here
    * it is added for clarity.
    */
   queue = (struct Queue *)malloc(sizeof(struct Queue));
   queue->samples = (float *)malloc(sizeof(float) * maxSize);
   queue->maxSize = maxSize;
   queue->head = 0;
   queue->tail = -1;
   queue->size = 0;

   return queue;
}


/*
 ********************************************************
 * Function: destroyQueue
 *
 * params:
 *    queue - pointer to queue to be freed
 * returns:
 *    None
 * Note:
 *    destroyQueue() does not check for null pointer
 *    as free() does not reqire it.
 *********************************************************
 */
void destroyQueue(struct Queue * queue)
{
   free(queue->samples);
   free(queue);
}


/*
 ********************************************************
 * Function: createQueue
 *
 * params:
 *   queue - queue to which to add the sample
 *   sample - value to be inserted
 *
 * returns:
 *     -1 if fail, 0 on success
 *
 ********************************************************
 */
int Enqueue(struct Queue * queue, float sample)
{
   if(!queue) {
      fprintf(stderr, "Null pointer passed to %s", __FUNCTION__);
      return -1;
   }
   if (queue->size < queue->maxSize) {
      queue->size++;
   }
   queue->tail++;
   if(queue->tail == queue->maxSize) {
      queue->tail = 0;
   }
   queue->samples[queue->tail] = sample;
   return 0;
}

/*
 ********************************************************
 * Function: showQueue
 *
 * params:
 *   queue - queue for which to display samples
 *
 * returns:
 *     -1 if fail, 0 on success
 *
 */

int showQueue(struct Queue * queue)
{
   if(!queue) {
      fprintf(stderr, "Null pointer passed to %s", __FUNCTION__);
      return -1;
   }
   for(int i = 0; i < queue->size; i++) {
      printf("%f\n", queue->samples[i]);
   }
   return 0;
}

