#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
#include "queue.h"
#include <math.h>

#define SLEEP_TIME 1 // Time in seconds
#define QUEUE_SIZE 10
#define BATCH_SIZE 10 // How many samples should be enqueued at once;

struct Queue * q;
float counter = 0.0;


/* Create mutex and conditional variable for synchronization */
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t queueCond = PTHREAD_COND_INITIALIZER;

void * producer();
void * consumer();

int main(void)
{
   pthread_t prodTh, consTh;

   q = createQueue(QUEUE_SIZE);

   if(!q){
      fprintf(stderr, "Wrong queue size\n");
      return -1;
   }

   /* Create threads */
   pthread_create(&prodTh, NULL, &producer, NULL);
   pthread_create(&consTh, NULL, &consumer, NULL);

   /* Tell main() to wait for the following threads to finish */
   pthread_join(prodTh, NULL);
   pthread_join(consTh, NULL);

   return 0;
}

void * producer()
{
   while(1){
   pthread_mutex_lock(&mutex);
   /* This loop enques BATCH numer of samples to the queue */
   for(int i = 0; i < BATCH_SIZE; i++){
   counter += 0.1;
   Enqueue(q,sin(counter));
   }
   /* Tell consumer that data is ready */
   pthread_cond_signal(&queueCond);
   pthread_mutex_unlock(&mutex);
   sleep(SLEEP_TIME);
   }
}

void * consumer()
{
   struct Queue consQ;
   while(1) {
      pthread_mutex_lock(&mutex);
      /* Wait until data is prepared by producer */
      pthread_cond_wait(&queueCond, &mutex);

      /* Consume data */
      consQ = *q;
      pthread_mutex_unlock(&mutex);

      /* Process consumed data outside of mutex lock */
      showQueue(&consQ);
      printf("\n");
   }
}
